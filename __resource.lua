client_scripts
{
	'vehicle.lua',
	'locks.lua',
	'engine.lua'
}

export 'getCarLock'
export 'setCarLock'
export 'getCarOwner'
