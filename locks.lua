local lockedDecorName = "_locked"
local ownerDecorName = "_owner"

math.randomseed(math.floor(GetRandomFloatInRange(1.0, 2147000000)))

DecorRegister(lockedDecorName, 2) -- bool decor
DecorRegister(ownerDecorName, 3) -- int decor

function getCarLock(vehicle)
	return DecorGetBool(vehicle, lockedDecorName)
end

function setCarLock(vehicle, locked)
	DecorSetBool(vehicle, lockedDecorName, locked)
	if locked then SetVehicleDoorsLocked(vehicle, 2); else SetVehicleDoorsLocked(vehicle, 0); end
end

function getCarOwner(vehicle)
	if not DecorExistOn(vehicle, ownerDecorName) then
		return nil
	end
	
	return DecorGetInt(vehicle, ownerDecorName)
end

function initLock(vehicle, owner)
	local chance = 70
	local locked = false
	
	if IsVehicleEngineOn(vehicle) then chance = 25; end
	
	if owner and owner ~= nil then chance = 0; end
	
	if chance >= math.random(1, 100) then locked = true; end
	
	if owner and owner ~= nil then DecorSetInt(vehicle, ownerDecorName, owner); else DecorSetInt(vehicle, ownerDecorName, -1); end
	
	setCarLock(vehicle, locked)
end

AddEventHandler('onTryingToEnterLockedVehicle', function(vehicle, door)
	Wait(750)
	ClearPedTasks(GetPlayerPed(-1))
end)

AddEventHandler('onTryingToEnterVehicle', function(vehicle)
	if not DecorExistOn(vehicle, lockedDecorName) then
		initLock(vehicle)
		TriggerEvent('chatMessage', "system", { 255, 0, 0 }, "initLock")
	else
		local locked = DecorGetBool(vehicle, lockedDecorName)
		if locked then SetVehicleDoorsLocked(vehicle, 2); else SetVehicleDoorsLocked(vehicle, 0); end
	end
	
	if not IsVehicleEngineOn(vehicle) then
		SetVehicleNeedsToBeHotwired(vehicle, true)
	end
	
end)

AddEventHandler('onVehicleEnter', function(vehicle)
	if GetPedInVehicleSeat(vehicle, -1) == ped then
		SetVehicleDoorShut(vehicle, 0, true)
	end
	
	if not DecorExistOn(vehicle, lockedDecorName) then -- vehicle must've been spawned, set player as owner
		initLock(vehicle, GetPlayerServerId(PlayerId()))
	else
		--SetVehicleUndriveable(vehicle, true)
		if DecorGetInt(vehicle, ownerDecorName) == GetPlayerServerId(PlayerId()) then -- player owns the vehicle
			SetVehicleNeedsToBeHotwired(vehicle, false)
		end
	end
end)