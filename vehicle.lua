local ped

local vehicle =
{
	id = nil,
	engineHealth = nil,
	petrolHealth = nil,
	entityHealth = nil,
	driver = nil
}

local prevEngineHealth = nil
local prevPetrolTankHealth = nil
local prevEntityHealth = nil

local enteringVehicle = false
local enteringLockedVehicle = false

function updateVehicle()
	if IsPedInAnyVehicle(ped, false) then
		
		local vehicleId = GetVehiclePedIsIn(ped, false)
		
		if vehicle.id ~= vehicleId then -- new vehicle
			prevEngineHealth = nil
			prevPetrolHealth = nil
			prevEntityHealth = nil
		
			TriggerEvent('onVehicleEnter', vehicleId)
		end
		
		vehicle.id = vehicleId
		
		vehicle.engineHealth = math.floor(GetVehicleEngineHealth(vehicle.id))
		vehicle.petrolHealth = math.floor(GetVehiclePetrolTankHealth(vehicle.id))
		vehicle.entityHealth = math.floor(GetEntityHealth(vehicle.id))
		vehicle.driver = GetPedInVehicleSeat(vehicle.id, -1)
		
		if IsPedJumpingOutOfVehicle(ped) then
			TriggerEvent('onVehicleExiting', vehicle.id)
		end
		
	else
		if vehicle.id ~= nil then -- exited vehicle
			prevEngineHealth = nil
			prevPetrolHealth = nil
			prevEntityHealth = nil
		
			TriggerEvent('onVehicleExit', vehicle.id)
			
			vehicle.id = nil
		else
			--check to see if ped is trying to enter a vehicle

			local veh = GetVehiclePedIsTryingToEnter(ped)
			
			if IsPedTryingToEnterALockedVehicle(ped) then
				if not enteringLockedVehicle then
					enteringLockedVehicle = true
					TriggerEvent('onTryingToEnterLockedVehicle', veh)
				end
			else
				if enteringLockedVehicle then
					enteringLockedVehicle = false
				end
				
				if veh and DoesEntityExist(veh) then
					if not enteringVehicle then
						enteringVehicle = true
						TriggerEvent('onTryingToEnterVehicle', veh)
					end
				else
					if enteringVehicle then
						enteringVehicle = false
					end
				end
				
			end
		end
	end
	
	if vehicle.id ~= nil then
		if prevEngineHealth == nil or prevPetrolHealth == nil or prevEntityHealth == nil then
			prevEngineHealth = vehicle.engineHealth
			prevPetrolHealth = vehicle.petrolHealth
			prevEntityHealth = vehicle.entityHealth
		end
	
		return true
	else
		return false
	end
end

Citizen.CreateThread(function()
	while true do
		ped = GetPlayerPed(-1)

		prevEngineHealth = vehicle.engineHealth
		prevPetrolHealth = vehicle.petrolHealth
		prevEntityHealth = vehicle.entityHealth

		if updateVehicle() and vehicle.driver == ped then
			
			if vehicle.entityHealth < prevEntityHealth then
				TriggerEvent('onVehicleEntityDamage', vehicle, prevEntityHealth - vehicle.entityHealth)
			end
			
			if vehicle.engineHealth < prevEngineHealth then
				TriggerEvent('onVehicleEngineDamage', vehicle, prevEngineHealth - vehicle.engineHealth)
			end
			
			if vehicle.petrolHealth < prevPetrolHealth then
				TriggerEvent('onVehiclePetrolDamage', vehicle, prevPetrolHealth - vehicle.petrolHealth)
			end
		
		end
		
		Citizen.Wait(0)
	end
end)