-- the amount of damage it takes to knockout the engine
local entityDamageKnockout = 50
local engineDamageKnockout = 30

-- the amount of time the engine will be knocked out for (in seconds)
local knockoutTimeLow = 1.0
local knockoutTimeHigh = 5.0

-- the amount of engine health after which the vehicle will be totalled
local engineBrokenHealth = 500

local vehicle = nil
local engineBroken = false
CreateThread(function()
	while true do
		if engineBroken then
			SetVehicleEngineOn(vehicle, false)
			SetVehicleUndriveable(vehicle, true)
		end
		Citizen.Wait(1)
	end
end)

local knockedOut = false
function knockoutEngine(vehicle)
	
	if knockedOut then return end
	knockedOut = true
	
	CreateThread(function()
	
		local t = math.floor(GetRandomFloatInRange(knockoutTimeLow, knockoutTimeHigh))
		
		Citizen.Trace("Vehicle engine disabled for " .. t .. " seconds")
		
		SetVehicleEngineOn(vehicle, false)
		
		local elapsed = 0.0
		
		while true do
			elapsed = elapsed + GetFrameTime()
			
			SetVehicleUndriveable(vehicle, true)
			Citizen.Wait(10)
			
			if elapsed >= t then break end
		end
		
		SetVehicleUndriveable(vehicle, false)
		
		knockedOut = false
	end)
	
end

AddEventHandler('onVehicleEntityDamage', function(vehicle, damage)
	if damage >= entityDamageKnockout then
		knockoutEngine(vehicle.id)
	end
end)

AddEventHandler('onVehicleEngineDamage', function(vehicle, damage)
	if damage >= engineDamageKnockout then
		knockoutEngine(vehicle.id)
	end
	
	if vehicle.engineHealth <= engineBrokenHealth then
		Citizen.Trace("The vehicle engine has completely broken")
		engineBroken = true
	end
	
end)

AddEventHandler('onVehicleEnter', function(veh)
	vehicle = veh
	if math.floor(GetVehicleEngineHealth(veh)) <= engineBrokenHealth then
		Citizen.Trace("This vehicle's engine has completely broken")
		engineBroken = true
	else
		engineBroken = false
	end
end)

AddEventHandler('onVehicleExit', function(vehicle)
	engineBroken = false
end)


